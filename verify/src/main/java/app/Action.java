package app;

class Action {

    private enum ACTION{
        TRANSFER,
        WITHDRAW,
    }

    private ACTION action;

    Action(String action)  {
        checkifActionIsEmptyOrNull(action);
        setActionValue(action);
    }

    private void setActionValue(String action) {
        switch (action) {
            case "transfer":
                this.action = ACTION.TRANSFER;
                break;
            case  "withdraw":
                this.action = ACTION.WITHDRAW;
                break;
            default:
                throw new RuntimeException("NO Action found");
        }
    }

    public String getAction() {
        switch (action) {
            case TRANSFER:
                return "transfer";
            case  WITHDRAW:
                return "withdraw";
            default:
                throw new RuntimeException("NO Action found");
        }
    }

    public boolean isActionValueEquals(String value) {
        return action.toString().equalsIgnoreCase(value);
    }

    private void checkifActionIsEmptyOrNull(String action) {
        if(action == null || action.isEmpty()) {
            throw new RuntimeException("action should not be null");
        }
    }
}
