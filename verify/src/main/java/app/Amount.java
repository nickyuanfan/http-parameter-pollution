package app;

import java.math.BigDecimal;
import java.math.BigInteger;

class Amount {

    private final BigInteger amount;
    private final int largestAmount = 1000000;

    Amount(String amount) {
        checkIfAmountIsNotNull(amount);
        checkIfAmountIsValid(amount);
        checkAmountIsNotNegative(amount);
        checkIfWithinRange(amount);
        this.amount = new BigInteger(amount);
    }

    @Override
    public String toString() {
        return this.amount.toString();
    }

    private void checkIfAmountIsValid(String amount) {
        if(!amount.matches("[0-9]+")){
            throw new RuntimeException("amount should be between 0-9");
        }
    }

    private void checkIfAmountIsNotNull(String amount) {
        if(amount == null || amount.isEmpty()) {
            throw new RuntimeException("amount is null/ empty");
        }
    }

    private void checkAmountIsNotNegative(String bigInteger) {
        BigInteger amount = new BigInteger(bigInteger);
        if (amount.compareTo(BigInteger.ZERO) < 0) {
            throw new RuntimeException("Negative number");
        }
    }

    private void checkIfWithinRange(String bigInteger) {
        BigInteger amount = new BigInteger(bigInteger);
        if (amount.compareTo(BigInteger.valueOf(largestAmount)) > 0 || new BigDecimal(amount).compareTo(BigDecimal.valueOf(Integer.MIN_VALUE)) < 0) {
            throw new RuntimeException("Exceeded Range");
        }
    }
}
