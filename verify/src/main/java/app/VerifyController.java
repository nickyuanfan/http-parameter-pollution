package app;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

@RestController
public class VerifyController {

    @Value("${paymenturl}")
    private String paymentUrl;

    @PostMapping("/")
    public String res(HttpServletRequest request) {
        final Action action = new Action(request.getParameter("action"));
        final Amount amount = new Amount(request.getParameter("amount"));
        if (action.isActionValueEquals("transfer")) {
            System.out.println("Verify Controller: Going to transfer $" + amount);
            RestTemplate restTemplate = new RestTemplate();
            String fakePaymentUrl = this.paymentUrl;  //Internal fake payment micro-service
            String url = fakePaymentUrl + "?action=" + action.getAction() + "&amount=" + amount.toString();
            ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
            return response.getBody();
        } else if (action.isActionValueEquals("withdraw")) {
            return "Verify Controller: Sorry, you can only make transfer";
        } else {
            return "Verify Controller: You must specify action: transfer or withdraw";
        }
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody
    String handleException(RuntimeException e) {
        return null;
    }

}
